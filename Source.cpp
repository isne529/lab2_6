#include <iostream>
#include <string>
#include "Graph.h"
using namespace std;

void main()
{
	int data[4][4] = { { 0,6,2,0 },{ 6,0,3,2 },{ 2,3,0,1 },{ 0,2,1,0 } };
	Graph g;
	g.insert(data);
	g.printGraph();
	if (g.pseudograph())
	{
		cout << "This graph is pseudograph" << endl;
	}
	else
	{
		cout << "This graph is not pseudograph" << endl;
	}

	g.dijkstra(data, 0);
	system("pause");

}